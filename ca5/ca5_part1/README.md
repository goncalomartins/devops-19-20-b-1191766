### **Class Assignment 5 - Part 1**

**1. The goal is to create a very simple pipeline (similar to the example from the
lectures).**

Instalar o jenkins:

Fazer download no seguinte link:

    https://www.jenkins.io/doc/book/installing/#war-file

Abrir a linha de comandos onde foi guardado o ficheiro _jenkins.war_
Executar o seguinte comando:
    
    java - jar jenkins.war
    
Ir a http://localhost:8080 copiar a password fornecida na execuçao do comando anterior
para criar uma conta no jenkins.

Depois da conta criada e necessario criar Credencias

Selecionar o campo 'Credencials' e de seguida global.

No _Username_ e _password_ e necessario colocar as usadas na conta bitbucket.

No _ID_ sera o nome de utilizador para aceder as informaçoes bitbucket atraves do jenkins,
que serao usadas posteriormente no jenkinsfile

**2. You should define the following stages in your pipeline:**

    Checkout. To checkout the code form the repository
    
    Assemble. Compiles and Produces the archive files with the application. Do not use
    the build task of gradle (because it also executes the tests)!
    
    Test. Executes the Unit Tests and publish in Jenkins the Test results. See the junit
    step for further information on how to archive/publish test results.
    
    Archive. Archives in Jenkins the archive files (generated during Assemble)
    
Para criar uma nova _pipeline_, e necessario, na pagina inicial selecionar **_New Item_** e depois
 selecionar **_Pipelie_**, dando-lhe um nome.
E necessario configurar o **_pipeline_** atraves **_Pipeline script_**, e as alteraçoes sao as seguintes:

    pipeline {
        agent any
        stages {
            stage('Checkout') {
                steps {
                    echo 'Checking out...'
                    git credentialsId: 'goncalomartins-bitbucket', url: 'https://goncalomartins@bitbucket.org/goncalomartins/devops-19-20-b-1191766.git'
                }
            }
            stage('Assemble') {
                steps {
                    echo 'Building...'
                    bat 'cd ca2/ca2_part1/gradle_basic_demo & gradlew clean assemble'
                }
            }
            stage ('Test'){
                steps {
                 echo 'Testing...'
                 bat 'cd ca2/ca2_part1/gradle_basic_demo & gradlew test'
                 junit 'ca2/ca2_part1/gradle_basic_demo/build/test-results/test/*.xml'
                }
            }
            stage('Archive') {
                steps {
                    echo 'Archiving...'
                    archiveArtifacts 'ca2/ca2_part1/gradle_basic_demo/build/distributions/*'
                }
            }
        }
    }
    
Guardar as alteraçoes e fazer **_Build Now_**.

Outro metodo e criar um ficheiro **_jenkinsfile_** onde sao colocados os _scripts_ e podem ser usados remotamente.
Para isso e necessario criar outro _**Item**_ e selecionar _**Pipeline**_

E necessario tambem editar a pagina de configuraçao do _**Job**_. Selecionar **_Pipeline script from SCM_**

No campo _**SCM**_ selecionar _**Git**_.

No campo _**Repositoy URL**_ e necessario colocar o URL para a nossa conta do _Bitbucket_:

    https://goncalomartins@bitbucket.org/goncalomartins/devops-19-20-b-1191766.git
    
No campo *Script Path* em que é necessário colocar o caminho para o ficheiro Jenkinsfile:

    ca5/ca5_part1/Jenkinsfile

Guardar as alteraçoes e fazer **_Build Now_**.

**3. At the end of the part 1 of this assignment mark your repository with the tag
ca5-part1.**

    git commit -a -m "fix #'nºcommit' 'mensagem' ”
    git push
    git tag ca5-part1
    git push origin ca5-part1