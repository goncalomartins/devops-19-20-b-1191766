### **Class Assignment 5 - Part 2**

**1. You should define the following stages in your pipeline:**

    Checkout. To checkout the code from the repository
    
    Assemble. Compiles and Produces the archive files with the application. Do not use
    the build task of gradle (because it also executes the tests)!
    
    Test. Executes the Unit Tests and publish in Jenkins the Test results. See the junit
    step for further information on how to archive/publish test results.
    
    Javadoc. Generates the javadoc of the project and publish it in Jenkins. See the
    publishHTML step for further information on how to archive/publish html reports.
    
    Archive. Archives in Jenkins the archive files (generated during Assemble, i.e., the war
    file)
    
    Publish Image. Generate a docker image with Tomcat and the war file and publish it
    in the Docker Hub.
    
Para criar uma nova _pipeline_, e necessario, na pagina inicial selecionar **_New Item_** e depois
 selecionar **_Pipelie_**, dando-lhe um nome.
E necessario configurar o **_pipeline_** atraves **_Pipeline script_**, e as alteraçoes sao as seguintes:

    pipeline {
        agent any
        stages {
            stage('Checkout') {
                steps {
                    echo 'Checking out...'
                    git credentialsId: 'goncalomartins-bitbucket', url: 'https://goncalomartins@bitbucket.org/goncalomartins/devops-19-20-b-1191766.git'
                }
            }
            stage('Assemble') {
                steps {
                    echo 'Building...'
                    bat 'cd ca2/ca2_part2v2/demo & gradlew clean assemble'
                }
            }
            stage ('Test'){
                steps {
                 echo 'Testing...'
                 bat 'cd ca2/ca2_part2v2/demo & gradlew test'
                 junit 'ca2/ca2_part2v2/demo/build/test-results/test/*.xml'
                }
            }
            stage ('Javadocs'){
                 steps {
                    echo 'Creating Javadocs'
                    bat 'cd ca2/ca2_part2v2/demo/ & gradlew javadoc'
    
                     publishHTML([
                             reportName: 'Javadoc',
                             reportDir: 'ca2/ca2_part2v2/demo/build/docs/javadoc/',
                             reportFiles: 'index.html',
                             keepAll: false,
                             alwaysLinkToLastBuild: false,
                             allowMissing: false
                     ])
                 }
            }
            stage('Archive') {
                steps {
                    echo 'Archiving...'
                    archiveArtifacts 'ca2/ca2_part2v2/demo/build/libs/*'
                }
            }
            stage ('Publish Image') {
                steps {
                    echo 'Publishing Docker image'
                    script {
                       docker.withRegistry('https://index.docker.io/v1/', 'goncalomartins-dockerHub') {
                                            def customImage =
                                            docker.build("goncalomartins/devops-19-20-b-1191766:{env.BUILD_ID}", "ca5/ca5_Part2")
                                            customImage.push()
                       }
                    }
                }
            }
        }
    }

Guardar as alteraçoes e fazer **_Build Now_**.

Para a **_Stage_** Javadocs, foi necessario intalar o plugin _**HTML Publisher**_ no jenkins.

Para a **_Stage_** Publish Image, foi necessario Criar uma Dockerfile, e foi usada a mesma que no Ca4, 
a qual foi feita uma copia para a para a pasta ca5-part2.
E tambem necessario que o Docker esteja a correr para este passo.

---

Outro metodo e criar um ficheiro **_jenkinsfile_** onde sao colocados os _scripts_ e podem ser usados remotamente.
Para isso e necessario criar outro _**Item**_ e selecionar _**Pipeline**_

E necessario tambem editar a pagina de configuraçao do _**Job**_. Selecionar **_Pipeline script from SCM_**

No campo _**SCM**_ selecionar _**Git**_.

No campo _**Repositoy URL**_ e necessario colocar o URL para a nossa conta do _Bitbucket_:

    https://goncalomartins@bitbucket.org/goncalomartins/devops-19-20-b-1191766.git
    
No campo *Script Path* em que é necessário colocar o caminho para o ficheiro Jenkinsfile:

    ca5/ca5_part2/Jenkinsfile

Guardar as alteraçoes e fazer **_Build Now_**.

**2. At the end of the part 2 of this assignment mark your repository with the tag
     ca5-part2.**
     
    git commit -a -m "fix #'nºcommit' 'mensagem' ”
    git push
    git tag ca5-part2
    git push origin ca5-part2