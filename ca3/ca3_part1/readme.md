### **Class Assignment 3 - Part 1**

**1. You should start by creating your VM as described in the lecture**

Estes sao os passos para criar uma VM usando o VirtualBox:

    Steps:
    1. Create VM
    2. Change VM settings:
        Connect image (ISO) with the Ubuntu 18.04 minimal installation media: 
        https://help.ubuntu.com/community/Installation/MinimalCD
        The VM needs 2024 MB RAM
        Set Network Adapter 1 as Nat _192.168.56.1_
        Set Network Adapter 2 as Host-only Adapter (vboxnet0)
    3. Start the VM and install Ubuntu
    
Depois da VM criada executar os seguintes comandos:

    sudo apt install net-tools
    sudo nano /etc/netplan/01-netcfg.yaml

Editar o ficheiro com os seguintes parametros:

    network:
      version: 2
      renderer: networkd
      ethernets:
        enp0s3:
          dhcp4: yes
        enp0s8:
          addresses:
            - 192.168.56.5/24
            
Apply the new changes:

    sudo netplan apply

Install openssh-server so that we can use ssh to open secure terminal sessions to the
VM (from other hosts)

    sudo apt install openssh-server

Enable password authentication for ssh

    sudo nano /etc/ssh/sshd_conﬁg
    uncomment the line PasswordAuthentication yes
    sudo service ssh restart

Install an ftp server so that we can use the FTP protocol to transfers files to/from
  the VM (from other hosts)
  
    sudo apt install vsftpd
    
Enable write access for vsftpd

    sudo nano /etc/vsftpd.conf
    uncomment the line write_enable=YES
    sudo service vsftpd restart
   
In the host (Windows or Linux or OSX), in a terminal/console, type:

    ssh goncalo@192.168.56.5
    
Instalar o Git e o Java

    sudo apt install git
    sudo apt install openjdk-8-jdk-headless

**2. You should clone your individual repository inside the VM**

    git clone https://goncalomartins@bitbucket.org/goncalomartins/devops-19-20-b-1191766.git
    

**3. You should try to build and execute the spring boot tutorial basic project and the
gradle_basic_demo project (from the previous assignments)**

Foi necessario instalar tambem o maven e o gradle:

    sudo apt install maven
    chmod +x mvnw		(para dar permissoes de execuçao)
    sudo apt install gradle
    chmod +x gradlew    (para dar permissoes de execuçao)
    
**4. For web projects you should access the web applications from the browser in your
host machine (i.e., the "real" machine)**

**CA1**

Ir para o diretorio ca1, basic, e correr o commando:

    ./mvnw spring-boot:run
    
Depois de terminado o _Build_ com sucesso verificar se funciona, colocar no browser http://192.168.56.5:8080/

**CA2**

CA2 part1

Ir para o diretorio ca2, ca2_part1, gradle_basic_demo,  e correr o commando:

    ./gradlew build (se necessario: chmod +x gradlew    (para dar permissoes de execuçao))
    
CA2 part2

Ir para o diretorio ca2, ca2_part2, demo,  e correr o commando:

    ./gradlew build (se necessario: chmod +x gradlew    (para dar permissoes de execuçao))
   
**5. At the end of the part 1 of this assignment mark your repository with the tag
ca3-part1.**
    
    git commit -a -m "fix #'nºcommit' 'mensagem' ”
    git push
    git tag ca3-part1
    git push origin ca3-part1
    

