### **Class Assignment 3 - Part 2**

**Analysis, design and implementation of the requirements**

**1. You should use https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/
as an initial solution**

**2. Study the Vagrantfile and see how it is used to create and provision 2 VMs:**
_web: this VM is used to run tomcat and the spring boot basic application_

_db: this VM is used to execute the H2 server database_

**3. Copy this Vagrantfile to your repository (inside the folder for this assignment)**

Fazer download do repositorio e copiar para a pasta ca3_part2 o ficheiro Vagrantfile, para esta 
ficar no repositorio a ser usado.

**4. Update the Vagrantfile configuration so that it uses your own gradle version of the
spring application**

A execução do git clone no vagrant para os repositórios privados pode falhar pois o git 
vai necessitar das vossas credenciais de acesso. Foi necessario colocar o repositorio publico
para resolver esse problema.

    # Change the following command to clone your own repository!
    git clone https://goncalomartins@bitbucket.org/goncalomartins/devops-19-20-b-1191766.git
    cd devops-19-20-b-1191766/ca2/ca2_part2v2/demo
    chmod +x gradlew
    ./gradlew clean build

**5. Check https://bitbucket.org/atb/tut-basic-gradle to see the changes
necessary so that the spring application uses the H2 server in the db VM. Replicate
the changes in your own version of the spring application.**

Foi criada uma copia do ca2_part2(ca2_part2v2) para ser modificada sem alterar 
o anteriormente feito.

**_5.1 ADD: Support for building war file_**

No ficheiro _build.gradle_ foi necessario adicionar o seguinte:

Adicionar na parte dos plugins:

    id 'war'

Na parte das dependencies:

    // To support war file for deploying to tomcat
    providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'

Criar uma Classe 'ServletInitializer.java' no caminho src/main/java/com/greglturnquist/payroll/
com as seguintes linhas de codigo:

    package com.greglturnquist.payroll;
    
    import org.springframework.boot.builder.SpringApplicationBuilder;
    import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
    
    public class ServletInitializer extends SpringBootServletInitializer {
    
        @Override
        protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {        return application.sources(ReactAndSpringDataRestApplication.class);
        }
    }
    
**_5.2 ADD: Support for h2 console._**

No ficheiro _application.properties_, situado em src/main/resources, adicionar as seguintes
linhas de codigo:

    spring.data.rest.base-path=/api
    spring.datasource.url=jdbc:h2:mem:jpadb
    spring.datasource.driverClassName=org.h2.Driver
    spring.datasource.username=sa
    spring.datasource.password=
    spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
    spring.h2.console.enabled=true
    spring.h2.console.path=/h2-console
    
**_5.3 ADD: weballowothers to h2._**

Ainda no finheiro _application.properties_ adicionar:

    spring.h2.console.settings.web-allow-others=true
    
**_5.4 ADD: Application context path._**

Ainda no finheiro _application.properties_ adicionar:

    server.servlet.context-path=/basic-0.0.1-SNAPSHOT
    
No ficheiro _app.js_ actualizar a seguinte linha:

    client({method: 'GET', path: '/basic-0.0.1-SNAPSHOT/api/employees'}).done(response => {
    
**_5.5 UPDATE: Fixes ref to css in index.html_**

No ficheiro _index.html_, em src/main/resources/templates, modificar a seguinte linha:

    <link rel="stylesheet" href="main.css" />
    
**_5.6 ADD: Settings for remote h2 database._**

Ainda no finheiro _application.properties_ adicionar:

    #spring.datasource.url=jdbc:h2:mem:jpadb
    spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/~/jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
    
**_5.7 UPDATE: Fixes h2 connection string._**

Ainda no finheiro _application.properties_ modificar:

    # In the following settings the h2 file is created in /home/vagrant folder
    spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE

**_5.8 UPDATE: So that spring will no drop de database on every execution_**

Ainda no finheiro _application.properties_ modificar:

    # So that spring will no drop de database on every execution.
    spring.jpa.hibernate.ddl-auto=update
    
_Entre cada passo foi feito commit para o repositorio._

Executar o comando _vagrant up_ no terminal, e apos isso confirmar atraves dos links a baixo se
tudo funciona.

    http://localhost:8080/basic-0.0.1-SNAPSHOT/
    http://192.168.33.10:8080/basic-0.0.1-SNAPSHOT/
    http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console
    http://192.168.33.10:8080/basic-0.0.1-SNAPSHOT/h2-console
    
**6. Describe the process in the readme file for this assignment.**

Nao me foi possivel correr o comando _vagrant up_ tive erros que nao consegui resolver.

**7. At the end of the part 2 of this assignment mark your repository with the tag
ca3-part2.**

    git commit -a -m "fix #'nºcommit' 'mensagem' "
    git push
    git tag ca3-part2
    git push origin ca3-part2