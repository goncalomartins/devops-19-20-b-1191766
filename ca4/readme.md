### **Class Assignment 4**

**1. You should produce a solution similar to the one of the part 2 of the previous CA
but now using Docker instead of Vagrant.**

Primeiro passo foi fazer download do repositorio exemplo:

**https://bitbucket.org/atb/docker-compose-spring-tut-demo/**

Copiar os ficheiros do repositorio para a pasta do Ca4

Editar o ficheiro Dockerfile(web):

    FROM tomcat
    RUN apt-get update -y
    RUN apt-get install -f
    RUN apt-get install git -y
    RUN apt-get install nodejs -y    
    RUN apt-get install npm -y    
    RUN mkdir -p /tmp/build    
    WORKDIR /tmp/build/    
    RUN git clone https://goncalomartins@bitbucket.org/goncalomartins/devops-19-20-b-1191766.git    
    WORKDIR /tmp/build/devops-19-20-b-1191766/ca2/ca2_part2v2/demo    
    RUN chmod u+x gradlew    
    RUN ./gradlew clean build    
    RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/    
    EXPOSE 8080

**2. You should use docker-compose to produce 2 services/containers:
     web: this container is used to run Tomcat and the spring application
     db: this container is used to execute the H2 server database**
     
Depois das ediçoes e com o Docker tool-box instalado, executar os seguintes comandos:

    docker-compose build
    docker-compose up
    
Para verificar se tudo correu bem aceder aos seguintes links:

    
    http://192.168.99.100:8080/demo-0.0.1-SNAPSHOT/             (website)
    http://192.168.99.100:8080/demo-0.0.1-SNAPSHOT/h2-console   (base de dados)
    
**3. Publish the images (db and web) to Docker Hub (https://hub.docker.com)**

    Criar Conta e fazer log in
    Clicar em Create Repository.
    Escolher um nome e uma descriçao para o repositorio e clicar em Create.
    Fazer log in no DockerHub a partir da linha de comandos e inserir as credencias necessarias:
        docker login
    Verificar o ID das imagens:
        docker images
    Fazer TAG a imagem: 
        docker tag <SOURCE_IMAGE[:TAG]> <TARGET_IMAGE[:TAG]>
    Fazer Push da imagem: 
        docker push <NAME[:TAG]>
        
**4. Use a volume with the db container to get a copy of the database file by using the
     exec to run a shell in the container and copying the database file to the volume.**
     
Neste passo foram executados os seguintes Comandos:

    docker-compose exec db bash
    cp /usr/src/app/jpadb.mv.db /usr/src/data
    
**5. Describe the process in the readme file for this assignment. Include all the Docker
     files in your repository (i.e., docker-compose.yml, Dockerfile).**

Este trabalho foi feito em conjunto com os meus colegas de grupo, pois eu nao sou capaz de criar maquinas virtuais
o que me dificulta fazer estas tarefas. Mas com a ajuda dos colegas, consegui acompanhar todos os passos e os problemas
que foram aparecendo.

**6. At the end of this assignment mark your repository with the tag ca4.**

    git commit -a -m "fix #'nºcommit' 'mensagem' ”
    git push
    git tag ca4
    git push origin ca4
