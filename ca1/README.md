###**1. Analysis, Design and Implementation**

**Tag the initial version as v1.2.0:**

    git tag -a v1.2.0 -m “first version”
    git push origin v1.2.0
    git push

**You should create a branch called email-field:**

    git checkout -b email-field; (Ao usar o '-b' este comando cria e muda para esse mesmo branch)


**You should add support for email field:**

Ir à classe Employee e adicionar um atributo novo String email e adicionar ao construtor;

    public Employee(String firstName, String lastName, String description, String email) {
            setFirstName(firstName);
            setLastName(lastName);
            setDescription(description);
            setEmail(email);
        }

Ir à classe DataBaseLoader e no método public void run adicionar o novo string do email;

    this.repository.save(new Employee("Frodo", "Baggins", "ring bearer", "frodo@email"));

Ir ao ficheiro app.js e adicionar
 
    <th>Email</th>
    <td>{this.props.employee.email}</td>

**You should also add unit tests for testing the creation of Employees and the validation of its attributes 
(for instance, no null/empty values):**

Adicionar na classe Employee os métodos Set com restrições para garantir que o atributo como input não pode ser null nem ter espaços vazios.
Ex.:

    public void setFirstName(String firstName) {
            if (firstName != null && !firstName.isEmpty()) {
                this.firstName = firstName;
            } else {
                throw new IllegalArgumentException("Input 'firstName' is invalid!");
            }
        }
-Criar classe de testes e fazer a devida validação dos métodos set, que em caso de nao cumprimento dos requisitos enviam uma excepçao.

    /**
     * Test for Employee Constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for Employee constructor - Happy case")
    void employeeConstructorHappyCaseTest() {
        Employee frodo = new Employee("Frodo", "Baggins", "ring bearer", "frodo@email");
        assertTrue(frodo instanceof Employee);
    }

    /**
     * Test for set firstName
     * Exception with invalid firstName - null case
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with null firstName")
    void employeeConstructorEnsureExceptionWithNullFirstNameTest() {
        assertThrows(IllegalArgumentException.class, () -> {
            Employee frodo = new Employee(null, "Baggins", "ring bearer", "frodo@email");
        });
    }

    /**
     * Test for set firstName
     * Exception with invalid firstName - empty case
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with empty firstName")
    void employeeConstructorEnsureExceptionWithEmptyFirstNameTest() {
        assertThrows(IllegalArgumentException.class, () -> {
            Employee frodo = new Employee("", "Baggins", "ring bearer", "frodo@email");
        });
    }



**You should debug the server and client parts of the solution:**

Verificar se a pagina foi devidamente alterada.

    cd basic
    mvnw spring-boot:run
    http://localhost:8080
    cd ..

**When the new feature is completed (and tested) the code should be merged with the master and a new tag should be created (e.g, v1.3.0).**

    git commit -a -m “fix #'nºcommit' 'mensagem' ”; // adiciona ficheiros e faz commit
    git push origin email-field;
    git checkout master;
    git merge email-field;
    git tag -a v1.3.0 -m “email field”;
    git push origin v1.3.0
    git push

**Create a branch called fix-invalid-email. The server should only accept Employees with a valid email 
(e.g., an email must have the "@" sign):**

    git checkout -b fix-invalid-email (Cria novo branch e muda para esse branch)

Ir à classe Employee e adicionar ao Metodo setEmail condiçoes que verifiquem que contem o @ e nao é vazio, nem null; 

    public void setEmail(String email) {
            if (email != null && !email.isEmpty() && email.contains("@")) {
                this.email = email;
            } else {
                throw new IllegalArgumentException("Input 'email' is invalid!");
            }
    
    }
    
Ir a Classe EmployeeTest e adicionar novo teste para testar nova condição

    /**
     * Test for set Email
     * Exception with invalid Email - without At Sign
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException without At sign email")
    void employeeConstructorEnsureExceptionWithoutAtSignTest() {
        assertThrows(IllegalArgumentException.class, () -> {
            Employee frodo = new Employee("Frodo", "Baggins", "ring bearer", "frodobaggins");
        });
    }

**You should debug the server and client parts of the solution.**

Verificar se a pagina foi devidamente alterada.

    cd basic
    mvnw spring-boot:run
    http://localhost:8080
    cd ..

**When the fix is completed (and tested) the code should be merged into master and a new tag should be created 
(with a change in the minor number, e.g., v1.3.0 -> v1.3.1)**

    git commit -a -m “fix #'nºcommit' 'mensagem' ”; // adiciona ficheiros e faz commit
    git push origin fix-invalid-email;
    git checkou master;
    git merge fix-invalid-email;
    git tag -a v1.3.1 -m “fixed email”;
    git push ori
    gin v1.3.1
    git push

**At the end of the assignment mark your repository with the tag ca1.**

Criar e editar o ficheiro README.md e enviar para o repositorio

    git commit -a -m “fix #'nºcommit' 'mensagem' ”;
    git tag -a ca1 -m “final version”;
    git tag push origin ca1
    git push

###**2. Analysis, Design and Implementation**

**Mercurial**

Existem varias alternativas ao Git, a que eu escolhi foi Mercurial.

Tanto o Git como o Mercurial sao DVCS(Distributed Version Control System), permitem ambos fazer 'clone' do repositorio completo para maquinas pessoais, fazer as alteraçoes necessarias e enviar de novo para o repositorio central. 

Git é mais complexo e contem mais comandos. É necessario que a equipa saiba utilizar bem para o usar corretamente e de forma eficaz.

Mercurial é mais simples, a sintax é simples e a documentaçao facil de entender.

Em termos de segurança, Git é melhor para utilizaores com mais experiencia, enquanto que o Mercurial e para utilizadores menos experientes. 

A grande diferença entre Git e Mercurial é a estrutura dos Branchs, sendo a do Git considerada melhor. Em contrapartida Mercurial é mais facil de aprender e usar, mas por vezes pode causar confusoes.

**Conlusao**

Ambos sao similares, e fazem ambos o trabalho que lhes é requerido. Sao desenhados para diferentes niveis de experiencia, Git e necessario mais experiencia, enquanto que o Mercurial seria bom para começar, para utilizadores com menos experiencia.

**Lista de comandos**

| Git commands   |      Task     |  Mercurial commands |
|----------|:-------------:|------:|
| git init |  Create a new local directory | hg init |
| git remote add origin server_URL |  Connect your local repository to a remote server |  hg push server_URL |
| git clone | Copy a remote repository to your local system | hg clone |
| git add filename | Add a specific file |    hg add filename |
| git add --all | Add all changes |  hg add  |
| git commit -m "message" | Commit changes locally | 	hg commit -m "message"  |
| git push | Push changes to your remote repository |  hg push |
| git status | List the status |  hg status |
| git checkout -b branch_name | Create a new branch | hg bookmark bookmark_name |
| git checkout branch_name | Switch from one branch |   hg bookmark bookmark_name |
| git branch | List all the branches |   hg bookmarks |
| git branch -d branch_name | Delete the feature branch |  hg bookmark -d bookmark_name |
| git push origin branch_name | Push the branch | hg push -B bookmark_name |
| git pull | Fetch and merge changes on the remote server to your working directory | hg pull -u |
| git merge | Merge two different revisions into one |  hg merge |
| git diff |Show all changes made since the last commit |  hg diff |

##**Implementaçao:**

**Tag the initial version as v1.2.0:**

    $ hg tag v1.2.0
    
**You should create a branch called email-field:**
    
    $ hg bookmark email-field
    
**When the new feature is completed (and tested) the code should be merged with the master and a new tag should be created (e.g, v1.3.0).**
  
    $ hg add
    $ hg commit -m "message"
    $ hg push -B email-field
    $ hg merge
    $ hg tag v1.3.0
    $ hg push
    
**Create a branch called fix-invalid-email. The server should only accept Employees with a valid email 
(e.g., an email must have the "@" sign):**
    
    $ hg bookmark fix-invalid-email
    
**When the fix is completed (and tested) the code should be merged into master and a new tag should be created 
(with a change in the minor number, e.g., v1.3.0 -> v1.3.1)**
    
    $ hg add 
    $ hg commit -m "message"
    $ hg push -B fix-invalid-email  
    $ hg merge
    $ hg tag v1.3.1
    $ hg push

###**Referencias**

https://confluence.atlassian.com/get-started-with-bitbucket/git-and-mercurial-commands-860009656.html

https://www.perforce.com/blog/vcs/git-vs-mercurial-how-are-they-different
