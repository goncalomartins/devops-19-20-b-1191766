package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class EmployeeTest {

    /**
     * Test for Employee Constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for Employee constructor - Happy case")
    void employeeConstructorHappyCaseTest() {
        Employee frodo = new Employee("Frodo", "Baggins", "ring bearer", "frodo@email");
        assertTrue(frodo instanceof Employee);
    }

    /**
     * Test for set firstName
     * Exception with invalid firstName - null case
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with null firstName")
    void employeeConstructorEnsureExceptionWithNullFirstNameTest() {
        assertThrows(IllegalArgumentException.class, () -> {
            Employee frodo = new Employee(null, "Baggins", "ring bearer", "frodo@email");
        });
    }

    /**
     * Test for set firstName
     * Exception with invalid firstName - empty case
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with empty firstName")
    void employeeConstructorEnsureExceptionWithEmptyFirstNameTest() {
        assertThrows(IllegalArgumentException.class, () -> {
            Employee frodo = new Employee("", "Baggins", "ring bearer", "frodo@email");
        });
    }

    /**
     * Test for set lastName
     * Exception with invalid lastName - null case
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with null lastName")
    void employeeConstructorEnsureExceptionWithNullLastNameTest() {
        assertThrows(IllegalArgumentException.class, () -> {
            Employee frodo = new Employee("Frodo", null, "ring bearer", "frodo@email");
        });
    }

    /**
     * Test for set lastName
     * Exception with invalid lastName - empty case
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with empty lastName")
    void employeeConstructorEnsureExceptionWithEmptyLastNameTest() {
        assertThrows(IllegalArgumentException.class, () -> {
            Employee frodo = new Employee("Frodo", "", "ring bearer", "frodo@email");
        });
    }

    /**
     * Test for set Description
     * Exception with invalid Description - null case
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with null description")
    void employeeConstructorEnsureExceptionWithNullDescriptionTest() {
        assertThrows(IllegalArgumentException.class, () -> {
            Employee frodo = new Employee("Frodo", "Baggins", null, "frodo@email");
        });
    }

    /**
     * Test for set Description
     * Exception with invalid Description - empty case
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with empty description")
    void employeeConstructorEnsureExceptionWithEmptyDescriptionTest() {
        assertThrows(IllegalArgumentException.class, () -> {
            Employee frodo = new Employee("Frodo", "Baggins", "", "frodo@email");
        });
    }

    /**
     * Test for set Email
     * Exception with invalid Email - null case
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with null email")
    void employeeConstructorEnsureExceptionWithNullEmailTest() {
        assertThrows(IllegalArgumentException.class, () -> {
            Employee frodo = new Employee("Frodo", "Baggins", "ring bearer", null);
        });
    }

    /**
     * Test for set Email
     * Exception with invalid Email - empty case
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with empty email")
    void employeeConstructorEnsureExceptionWithEmptyEmailTest() {
        assertThrows(IllegalArgumentException.class, () -> {
            Employee frodo = new Employee("Frodo", "Baggins", "ring bearer", "");
        });
    }

    /**
     * Test for set Email
     * Exception with invalid Email - without At Sign
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException without At sign email")
    void employeeConstructorEnsureExceptionWithoutAtSignTest() {
        assertThrows(IllegalArgumentException.class, () -> {
            Employee frodo = new Employee("Frodo", "Baggins", "ring bearer", "frodobaggins");
        });
    }

}