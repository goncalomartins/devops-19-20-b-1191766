### **Class Assignment 2 - Part 1**

**Analysis, design and implementation of the requirements**

**1. You should start by downloading and commit to your repository (in a folder for Part
1 of CA2)**

    git commit -A -m "ref #number 'message'"
    git push

**2. Build**

To build a .jar file with the application:

    gradlew build

**Run the server**

Open a terminal and execute the following command from the project's root directory:

    java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

**Run a Client**

Open another terminal and execute the following gradle task from the project's root directory:

    gradlew runClient

After the successful build message, a chat window is opened. 

**3. Add a new task to execute the server.**

No ficheiro build.gradle inserir o segunte codigo:

    task executeServer(type: JavaExec, dependsOn: classes) {
        
        description = "Executes chat server on localhost:59001 "
    
        classpath = sourceSets.main.runtimeClasspath
    
        main = 'basic_demo.ChatServerApp'
    
        args '59001'
    }
    
Para confirmar que o codigo esta correto:

    gradlew tasks --all (verifica as tasks disponiveis)
    gradlew executeServer (faz com que a task seja executada)
    
    
Fazer commit e push para o repositorio.
    
**4. Add a simple unit test and update the gradle script so that it is able to execute the
  test.**
  
Para adicionar os testes foi necessario criar _src/test/java/basic_demo/AppTest_ e dentro desta class copiar o codigo 
fornecido nos slides.

Foi necessario adicionar novas dependencias para o junit 4.12

    dependencies {
        // Use Apache Log4J for logging
        implementation group: 'org.apache.logging.log4j', name: 'log4j-api', version: '2.11.2'
        implementation group: 'org.apache.logging.log4j', name: 'log4j-core', version: '2.11.2'
        testCompile group: 'junit', name: 'junit', version: '4.12'
    }

Depois de correr os testes fazer commit e push para o repositorio.

**5. Add a new task of type Copy to be used to make a backup of the sources of the application.**
  
Para isto foi adicionado o seguinte codigo em _build.gradle_:

    task backup (type: Copy){
           from 'src'
           into 'backup'
         }

Esta task copia todos os ficheiros de _src_ para a pasta _backup_, previamente criada.

Fazer commit e push para o repositorio.

**6. Add a new task of type Zip to be used to make an archive (i.e., zip file) of the sources of the application.**

Para isto foi adicionado o seguinte codigo em _build.gradle_:

    task zipFile (type:Zip) {
        from 'src'
        archiveFileName = 'src.zip'
        destinationDirectory = file('zipfiles')
    }

Esta task copia todos os ficheiros, em formato _.zip_ de _src_ para a pasta _zipfiles_, previamente criada.

Fazer commit e push para o repositorio.
    
**7. At the end of the part 1 of this assignment mark your repository with the tag
  ca2-part1.**
  
Foi usado o Git para adicionar os ficheiros, fazer os commits e push para o repositorio.

Editar o ficheiro README.md, adicionar ao repositorio e fazer a tag ca2-part1

    git commit -A -m "fix #number 'message'"
    git push

    git tag ca2-part1
    git push origin ca2-part1
